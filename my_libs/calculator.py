#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json


class Calculator:
    __slots__ = ["dresources", "drecipes", "dminers", "dbuildings", "resources_dict", "resources_key_name", "res", "deep"]

    def __init__(self):
        self.res = []
        self.dresources = []
        self.resources_dict = {}
        self.drecipes = []
        self.dminers = []
        self.dbuildings = []
        self.deep = 0
        self.get_data()
        self.resources_key_name = [r['key_name'] for r in self.dresources]

    def get_data(self):
        with open("data/data.json", "r") as f:
            data = json.load(f)
        self.dresources = data["resources"]
        self.drecipes = data["recipes"]
        self.dminers = data["miners"]
        self.dbuildings = data["buildings"]

    def needed_to_prod(self, recipe_name, prd_min_target=1):
        self.deep += 1
        try:
            recipe = [r for r in self.drecipes if r["name"] == recipe_name][0]
        except IndexError:
            return f"you search '{ recipe_name }', but this not exist in database"

        ingredients = recipe["ingredients"]

        for ingredient in ingredients:
            ing_name = " ".join(str.capitalize(e) for e in ingredient[0].split('-'))
            if "High" in ing_name and "Speed" in ing_name:
                ing_name = ing_name.replace("High Speed", "High-Speed")
            if ingredient[0] not in self.resources_key_name:
                self.needed_to_prod(ing_name, ingredient[1] * prd_min_target)
                self.deep -= 1

            if ing_name in self.resources_dict.keys():
                self.resources_dict[ing_name] += ingredient[1] * prd_min_target
            else:
                self.resources_dict[ing_name] = ingredient[1] * prd_min_target

    def dev(self, ntp_recipe_name, ntp_prd_min_target=1):
        self.needed_to_prod(ntp_recipe_name, ntp_prd_min_target)

        try:
            recipe = [r for r in self.drecipes if r["name"] == ntp_recipe_name][0]
        except IndexError:
            return f"you search '{ ntp_recipe_name }', but this not exist in database"

        n_craft_by_run = recipe["product"][1]
        n_run = 60 / recipe["time"]

        needed_run = ntp_prd_min_target / n_craft_by_run

        #
        # if self.deep == 1:
        #     stred_resource_dict = " ".join(f"{v} {k}," for k, v in self.resources_dict.items())
        #     self.res = f"time to product {ntp_prd_min_target} {recipe['name']}: {time_to_prd} minutes with " \
        #                f"{prd_min} {recipe['name']}/min and " + stred_resource_dict
        #
        #     return self.res
