#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from flask import Flask, render_template, request
from my_libs import Calculator

app = Flask(__name__)


@app.route('/')
def index():
    a = Calculator()
    return render_template('base.html', len=len(a.drecipes), recipes=a.drecipes)


@app.route('/dev', methods=['GET', 'POST'])
def dev():
    a = Calculator()
    f = request.form
    z = a.dev(f['recipe'], int(f['n']))
    return render_template('res.html', res=z, recipe=f['recipe'])


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
